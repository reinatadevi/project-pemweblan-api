<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Requests\StoreComment;

class CommentController extends Controller
{
    /**
     * Create CommentController instance.
     * 
     * @return void
     */
    public function __construct()
    {
       // $this->authorizeResource(Comment::class, 'comment');
    }

    /**
     * Display a listing of article's comment.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Article $article
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Article $article)
    {
        $sortBy = $request->query('sortby') == 'oldest' ? 'ASC' : 'DESC';

        $comments = $article
            ->comments()
            ->orderBy('created_at', $sortBy)
            ->paginate(10);

        return rest_api('OK', $comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreComment  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComment $request)
    {
        $data = $request->only('content', 'article_id');
        $data['user_id'] = auth()->user()->id;

        $comment = Comment::create($data);
        return rest_api('Comment created!', $comment, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        $comment->load('user');
        return rest_api('OK', $comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        if ($comment->user_id != auth()->user()->id) {
            return rest_error('Access denied!');
        }

        $comment->delete();
        return rest_api('Comment deleted!');
    }
}
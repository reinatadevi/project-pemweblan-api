<?php

/**
 * Create api response
 * 
 * @param string message
 * @param mixed data
 * @param integer response code
 * 
 * @return Response
 */
if (!function_exists('rest_api')) {
    function rest_api($message, $data = NULL, $code = 200)
    {
        $response = [
            'message' => $message,
            'data'    => $data,
        ];

        return response()->json($response, $code);
    }
}

/**
 * Create error response
 * 
 * @param string message
 * @param mixed additional data
 * @param integer response code
 * 
 * @return Response
 */
if (!function_exists('rest_error')) {
    function rest_error($message, $errors = null, $code = 400)
    {
        $response = [
            'message'   => $message,
            'errors'    => $errors
        ];

        return response()->json($response, $code);
    }
}

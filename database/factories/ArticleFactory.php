<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title'         => $faker->name,
        'content'       => $faker->sentence(),
        'user_id'       => 1,
        'category_id'   => $faker->numberBetween(1, 6)
    ];
});

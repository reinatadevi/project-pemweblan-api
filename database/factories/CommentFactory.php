<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'content'       => $faker->sentence(),
        'user_id'       => 1,
        'article_id'    => $faker->numberBetween(1, 50)
    ];
});

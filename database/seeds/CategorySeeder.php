<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'id' => 1,
            'name' => 'Lifestyle'
        ]);
        Category::create([
            'id' => 2,
            'name' => 'Showbiz'
        ]);
        Category::create([
            'id' => 3,
            'name' => 'Politik'
        ]);
        Category::create([
            'id' => 4,
            'name' => 'Sport'
        ]);
        Category::create([
            'id' => 5,
            'name' => 'Bisnis'
        ]);
        Category::create([
            'id' => 6,
            'name' => 'Otomotif'
        ]);
    }
}
